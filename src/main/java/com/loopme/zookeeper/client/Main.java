package com.loopme.zookeeper.client;

import com.loopme.zookeeper.client.properties.PropertiesHolder;

/**
 * @author IAsmolov on 11.07.17.
 */
public class Main {

	private static final String SERVICE1_HOST_PROPERTY = "service1.host";
	private static final String FLAG1_PROPERTY = "flag1";
	private static final String SOME_DATA_PROPERTY = "somedata";

	public static void main(String[] args) throws InterruptedException {
		String property;
		while (true) {
			property = (String) PropertiesHolder.getInstance().getPropetry(SERVICE1_HOST_PROPERTY);
			System.out.println("service host property =" + property);

			property = (String) PropertiesHolder.getInstance().getPropetry(FLAG1_PROPERTY);
			System.out.println("flag1 property =" + property);

			property = (String) PropertiesHolder.getInstance().getPropetry(SOME_DATA_PROPERTY);
			System.out.println("somedata property =" + property);

			property = (String) PropertiesHolder.getInstance().getPropetry("data2", "/zk_test1");
			System.out.println("somedata property =" + property);


			PropertiesHolder.getInstance().updateProperty(SOME_DATA_PROPERTY, property+"1");
			Thread.sleep(10000);
		}
	}
}

