package com.loopme.zookeeper.client.properties;

import com.loopme.zookeeper.client.ZooKeeperClient;

/**
 * @author IAsmolov on 11.07.17.
 */
public class PropertiesHolder {

	private static PropertiesHolder propertiesHolder;
	private ZooKeeperClient connection;
	private static final String DEFAULT_PATH_TO_PROPS = "/zk_test";
	private static final String DEFAULT_ZK_HOST = "localhost";
	private static final int DEFAULT_ZK_PORT = 2181;
	private static final String ZK_HOST_PORT = DEFAULT_ZK_HOST + ":" + DEFAULT_ZK_PORT;

	private PropertiesHolder() {
		try {
			connection = new ZooKeeperClient(DEFAULT_PATH_TO_PROPS, ZK_HOST_PORT);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static PropertiesHolder getInstance() {
		if (propertiesHolder == null) {
			propertiesHolder = new PropertiesHolder();
		}
		return propertiesHolder;
	}

	/**
	 * Get property value by name
	 * @param propertyName - property name
	 * @return property value
	 */
	public Object getPropetry(String propertyName) {
		return getPropetry(propertyName, DEFAULT_PATH_TO_PROPS);
	}

	public Object getPropetry(String propertyName, String pathToProps) {
		try {
			return connection.getProperty(propertyName, pathToProps);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * This method only for demonstration
	 * @param propertyName
	 * @param propertyValue
	 */
	public void updateProperty(String propertyName, String propertyValue) {
		connection.updateProperty(propertyName, propertyValue);
	}
}
