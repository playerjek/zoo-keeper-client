package com.loopme.zookeeper.client.properties;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class PropertiesUtils {

	private static final String SEPARATOR = ";";

	private PropertiesUtils(){}

	public static Map<String, Object> parseProps(String props) {
		return parseProps(props, SEPARATOR);
	}
	/**
	 * Parse input line with properties by given separator
	 * @param props
	 * @param separator
	 * @return PropertiesUtils
	 */
	public static Map<String, Object> parseProps(String props, String separator) {
		List<String> strings = Arrays.asList(props.split(separator));
		Map<String, Object> properties = new ConcurrentHashMap<>();
		for (String str: strings) {
			String[] keyValue = str.split("=");
			properties.put(keyValue[0], keyValue[1]);
		}
		return properties;
	}
}
