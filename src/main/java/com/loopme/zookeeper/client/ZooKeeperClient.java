package com.loopme.zookeeper.client;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import org.apache.commons.lang.StringUtils;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.Stat;

import com.loopme.zookeeper.client.properties.PropertiesUtils;

/**
 * ZooKeeperClient - used for creating connection and getting/updating properties in ZooKeeper
 */
public class ZooKeeperClient {

	private ZooKeeper zoo;
	private final CountDownLatch connectedSignal = new CountDownLatch(1);
	private String zkPathToProps;
	private Map<String, AbstractMap.SimpleEntry<Integer, Map<String, Object>>> propertiesCache = new HashMap<>();

	/**
	 * connection to ZooKepper
	 * @param zkPathToProps
	 * @param host
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public ZooKeeperClient(String zkPathToProps, String host) throws IOException, InterruptedException {
		this.zkPathToProps = zkPathToProps;
		zoo = new ZooKeeper(host,5000, we -> {

			if (we.getState() == Watcher.Event.KeeperState.SyncConnected) {
				connectedSignal.countDown();
			}
		});

		connectedSignal.await();
	}


	// Method to disconnect from zookeeper server
	public void close() throws InterruptedException {
		zoo.close();
	}

	public Map<String, Object> getProps() throws KeeperException, InterruptedException, UnsupportedEncodingException {
		return getProps(zkPathToProps);
	}

	public Map<String, Object> getProps(final String path) throws KeeperException, InterruptedException, UnsupportedEncodingException {
		Map<String, Object> data = null;
		if (StringUtils.isNotBlank(path)) {
			Stat stat = zoo.exists(path, true);
			if (stat == null) {
				return data;
			}
			if (!propertiesCache.containsKey(path) || propertiesCache.get(path).getKey() != stat.getVersion()) {
				byte[] nodeData = zoo.getData(path, we -> {
					if (we.getType() == Watcher.Event.EventType.None) {
						switch (we.getState()) {
							case Expired:
								connectedSignal.countDown();
								break;
						}
					} else {

						try {
							Stat stat1 = zoo.exists(path, true);
							if (stat1 != null) {
								byte[] bn = zoo.getData(path, false, null);
								String data1 = new String(bn, "UTF-8");
								propertiesCache.put(path, new AbstractMap.SimpleEntry<>(stat1.getVersion(), PropertiesUtils.parseProps(data1)));
								connectedSignal.countDown();
							}
						} catch (Exception ex) {
							System.out.println(ex.getMessage());
						}
					}
				}, null);
				String nodeDataStr = new String(nodeData, "UTF-8");
				System.out.println(nodeDataStr);
				connectedSignal.await();
				propertiesCache.put(path, new AbstractMap.SimpleEntry<>(stat.getVersion(), PropertiesUtils.parseProps(nodeDataStr)));
			}
			data = propertiesCache.get(path).getValue();
		}
		return data;
	}

	public void updateProperty(String propertyName, String propertyValue) {
		updateProperty(zkPathToProps, propertyName, propertyValue);
	}

	public void updateProperty(String path, String propertyName, String propertyValue) {
		try {
			Stat stat = zoo.exists(path, true);
			if (stat != null) {
				Map<String, Object> data = propertiesCache.get(path).getValue();
				List<String> resultData = new ArrayList<>();
				for (Map.Entry prop : data.entrySet()) {
					if (((String) prop.getKey()).contains(propertyName)) {
						resultData.add(propertyName + "=" + propertyValue);
					} else {
						resultData.add(prop.getKey() + "=" + prop.getValue());
					}
				}

				zoo.setData(path, buildData(resultData), stat.getVersion());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Object getProperty(String propertyName) throws InterruptedException, UnsupportedEncodingException, KeeperException {
		return getProperty(propertyName, zkPathToProps);
	}

	public Object getProperty(String propertyName, String path) throws InterruptedException, UnsupportedEncodingException, KeeperException {
		Map<String, Object> props = getProps(path);
		return props != null? props.get(propertyName) : null;
	}

	private byte[] buildData(List<String> data) {
		return StringUtils.join(data, ";").getBytes();
	}
}